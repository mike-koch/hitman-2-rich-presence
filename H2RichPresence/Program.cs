﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;
using DiscordRPC;
using DiscordRPC.Logging;
using Steamworks;

namespace H2RichPresence
{
    class Program
    {
        private static DiscordRpcClient discordClient;

        static void Main(string[] args)
        {
            // Blocking function until H2 has started
            WaitForHitman2Process();

            if (!InitializeSteamApi()) return;

            var steamId = SteamUser.GetSteamID();
            Console.WriteLine("Steam User: " + SteamFriends.GetPersonaName());

            Console.WriteLine("Process running. Press Esc to exit.");

            var lastCheckedTime = 0L;
            var lastStatus = string.Empty;
            
            while (true)
            {
                var hitmanProcesses = Process.GetProcessesByName("HITMAN2");
                if (hitmanProcesses.Length == 0)
                {
                    Console.WriteLine("HITMAN 2 is no longer running. Exiting.");
                    break;
                }

                var currentTime = GetCurrentTimeInMillis();

                if (currentTime - lastCheckedTime > 5000)
                {
                    var richPresence = SteamFriends.GetFriendRichPresence(steamId, "status");
                    if (lastStatus != richPresence)
                    {
                        ProcessRichPresence(richPresence);
                    }

                    lastStatus = richPresence;
                    lastCheckedTime = currentTime;
                }

                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo info = Console.ReadKey(true);

                    if (info.Key == ConsoleKey.Escape)
                    {
                        break;
                    }
                }

                Thread.Sleep(100);
            }

            SteamAPI.Shutdown();
        }

        private static void WaitForHitman2Process()
        {
            var hitmanProcesses = Process.GetProcessesByName("HITMAN2");
            var firstNope = true;
            while (hitmanProcesses.Length == 0)
            {
                if (firstNope)
                {
                    Console.WriteLine("Could not find HITMAN 2 running.");
                    firstNope = false;
                }
                Thread.Sleep(1000);
                hitmanProcesses = Process.GetProcessesByName("HITMAN2");
            }
        }

        private static bool InitializeSteamApi()
        {
            try
            {
                if (!SteamAPI.Init())
                {
                    Console.WriteLine("SteamAPI.Init() Failed!");
                    return false;
                }
            }
            catch (DllNotFoundException e)
            {
                Console.WriteLine(e);
                return false;
            }

            if (!Packsize.Test())
            {
                Console.WriteLine("You're using the wrong Steamworks.NET Assembly for this platform!");
                return false;
            }

            if (!DllCheck.Test())
            {
                Console.WriteLine("You're using the wrong dlls for this platform!");
                return false;
            }

            return true;
        }

        private static long GetCurrentTimeInMillis()
        {
            return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }

        private static void ProcessRichPresence(string richPresence)
        {
            if (discordClient == null)
            {
                discordClient = new DiscordRpcClient("543958719382290472")
                {
                    Logger = new ConsoleLogger
                    {
                        Level = LogLevel.Info
                    }
                };
                discordClient.Initialize();
            }

            var presence = new RichPresence()
            {
                Assets = new Assets
                {
                    LargeImageKey = "logo"
                },
                Details = richPresence
            };

            /*
             * Depending on the current status, broadcast to Discord differently (// indicates line break):
             *
             * "Playing {Game Mode} in {Location} | {Mission Name}"  ->  "Playing {Game Mode} in {Location}" // {Mission Name}"
             * "Playing {Game Mode} in {Location}" -> "Playing {Game Mode} // {Location}"
             *
             */
            //Playing Mission in Paris | The Showstopper
            var missionNameRegex = @"(.+) \| (.+)";
            var richPresenceRegex = "(.+) in (.+)";

            var match = Regex.Match(richPresence, richPresenceRegex, RegexOptions.IgnoreCase);

            if (match.Success)
            {
                presence.Details = match.Groups[1].Value.Replace("['UI_PRESENCE_UNKNOWN_KEY']", "Ghost Mode");
                var locationAndPossiblyMission = match.Groups[2].Value;
                presence.State = locationAndPossiblyMission;

                var missionMatch = Regex.Match(locationAndPossiblyMission, missionNameRegex, RegexOptions.IgnoreCase);
                if (missionMatch.Success)
                {
                    var missionName = missionMatch.Groups[2].Value;
                    presence.Assets.LargeImageKey = missionName.Replace(" ", "-").ToLowerInvariant();
                    presence.Assets.LargeImageText = missionName;

                    /*var sniperAssassinMissions = new List<string>
                    {
                        "The Last Yardbird",
                        "The Pen and the Sword",
                        "Crime and Punishment"
                    };*/
                    //presence.Assets.SmallImageKey = sniperAssassinMissions.Contains(missionName) ? "sniper-assassin" : "mission";
                    //presence.Assets.SmallImageText = sniperAssassinMissions.Contains(missionName) ? "Sniper Assassin" : "Mission";
                }
                else
                {
                    var locationName = locationAndPossiblyMission;
                    presence.Assets.LargeImageKey = locationName.Replace(" ", "-").ToLowerInvariant();
                    presence.Assets.LargeImageText = locationName;

                    //var gameMode = match.Groups[2].Value;
                    //presence.Assets.SmallImageKey = gameMode.Replace(" ", "-").ToLowerInvariant();
                    //presence.Assets.SmallImageText = gameMode;
                }
            }

            Console.WriteLine(match.Groups[0].Value);

            //var missionMatch = Regex.Match(match.Groups[2].Value, missionNameRegex, RegexOptions.IgnoreCase);

            
            discordClient.SetPresence(presence);
            discordClient.Invoke();
        }
    }
}
